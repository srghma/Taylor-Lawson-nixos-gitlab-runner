{
  nixpkgs ? import ./nix/nixpkgs
}:

let
  pkgs =
    let
      config = { allowUnfree = true; };
    in import nixpkgs { inherit config; };
in

pkgs.stdenv.mkDerivation (rec {
  name = "env";

  buildInputs = with pkgs; [
  ];

  NIX_PATH = pkgs.lib.concatStringsSep ":" [
    "nixpkgs=${nixpkgs}"
  ];

  HISTFILE = toString ../.bash_hist;
})
